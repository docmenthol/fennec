pub struct PanelFrame {
    frame_buffer: FrameBuffer,
    panel: u8,
}

pub type FrameBuffer = [u8; 8];

pub struct Frame {
    frames: [PanelFrame; 7],
    duration: u8,
}
