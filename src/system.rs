use crate::os::{Application, Os};

pub fn bootloader<T: Default + Application>() {
    let application = T::default();
    Os::new().application(application).boot().run();
}

pub fn panic_handler() -> ! {
    loop {} // :(){ :|:& };:
}
