// use crate::frame::Frame;

#[derive(Debug)]
pub struct Animation<N: Eq> {
    pub name: N,
    // pub frames: &'static [Frame],
    pub goto: Option<N>,
}
