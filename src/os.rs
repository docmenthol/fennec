use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    pac,
    watchdog::Watchdog,
};
use defmt::*;
use defmt_rtt as _;
use fugit::{Duration, Instant};
use pimoroni_tiny2040 as bsp;

#[allow(dead_code)]
#[derive(Default, Debug, Clone, Copy)]
pub enum Fps {
    Slow = 8,
    Cinema = 24,
    Thirty = 30,
    Fifty = 50,
    #[default]
    Sixty = 60,
    OneTwenty = 120,
    TwoFortyFour = 244,
}

pub trait Application {
    fn setup(self) -> Self;
    fn process(self, dt: u32) -> Self;
}

#[derive(Default, Debug)]
pub struct PartialOs<A: Application> {
    application: Option<A>,
    fps: Option<Fps>,
}

impl<A: Application> PartialOs<A> {
    pub fn default() -> PartialOs<A> {
        PartialOs {
            application: None,
            fps: None,
        }
    }

    pub fn build(self) -> Os<A> {
        match (self.application, self.fps) {
            (Some(application), Some(fps)) => Os { application, fps },
            (Some(application), None) => Os {
                application,
                fps: Fps::Sixty,
            },
            (None, _) => {
                self::panic!("kernal panic: tried to boot os without application");
            }
        }
    }

    pub fn fps(self, fps: Fps) -> Self {
        PartialOs {
            application: self.application,
            fps: Some(fps),
        }
    }

    pub fn application(self, application: A) -> Self {
        PartialOs {
            application: Some(application),
            fps: self.fps,
        }
    }

    pub fn boot(self) -> Os<A> {
        self.build()
    }
}

pub struct Os<A: Application> {
    application: A,
    fps: Fps,
}

impl<A: Application> Os<A> {
    pub fn new() -> PartialOs<A> {
        PartialOs::default()
    }

    pub fn run(mut self) -> ! {
        info!("application run");
        // Docs: https://docs.rs/rp2040-hal/latest/rp2040_hal/timer/struct.CountDown.html

        let mut pac = pac::Peripherals::take().unwrap();
        let core = pac::CorePeripherals::take().unwrap();
        let mut watchdog = Watchdog::new(pac.WATCHDOG);

        let clocks = init_clocks_and_plls(
            bsp::XOSC_CRYSTAL_FREQ,
            pac.XOSC,
            pac.CLOCKS,
            pac.PLL_SYS,
            pac.PLL_USB,
            &mut pac.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        let mut delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

        let mut dt = Duration::<u32, 1, 1_000>::from_ticks(0);
        let frame_duration = Duration::<u32, 1, 1_000>::millis(1000 / self.fps as u32);

        self.application = self.application.setup();

        info!("application setup complete");

        loop {
            let frame_start_time = Instant::<u32, 1, 1_000>::from_ticks(1);
            let frame_duration_target = frame_start_time + frame_duration;

            self.application = self.application.process(dt.to_micros());

            let frame_finish_time = Instant::<u32, 1, 1_000>::from_ticks(1);
            dt = frame_duration_target - frame_finish_time;

            delay.delay_us(dt.to_micros());
        }
    }
}
