#![no_std]
#![no_main]
mod protogen;

use crate::protogen::Protogen;
use bsp::hal::{pac, pac::interrupt, sio::Sio};
use core::panic::PanicInfo;
use fennec::os::Os;
use fennec::system::panic_handler;
use pimoroni_tiny2040 as bsp;
use rp2040_hal::gpio::Interrupt::EdgeLow;

fn reboot_to_flash_mode() {
    rp2040_hal::rom_data::reset_to_usb_boot(0, 0);
}

#[no_mangle]
pub extern "C" fn main() {
    let mut pac = pac::Peripherals::take().unwrap();
    let sio = Sio::new(pac.SIO);
    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let button_pin = pins.bootsel.into_pull_up_input();
    button_pin.set_interrupt_enabled(EdgeLow, true);

    let application = Protogen::new().build();
    Os::new().application(application).boot().run();
}

#[panic_handler]
fn panic(_panic: &PanicInfo) -> ! {
    panic_handler();
}

#[interrupt]
fn IO_IRQ_BANK0() {
    reboot_to_flash_mode();
}
