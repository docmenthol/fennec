use fennec::{animation::Animation, os::Application};

#[derive(Default, PartialEq, Eq)]
pub enum State {
    #[default]
    Idle,
    Blink,
}

#[derive(Default)]
pub struct PartialProtogen {
    state: Option<State>,
    current_animation: Option<Animation<State>>,
}

impl PartialProtogen {
    pub fn state(self, state: State) -> PartialProtogen {
        PartialProtogen {
            state: Some(state),
            current_animation: self.current_animation,
        }
    }

    pub fn animation(self, animation: Animation<State>) -> PartialProtogen {
        PartialProtogen {
            state: self.state,
            current_animation: Some(animation),
        }
    }

    pub fn build(self) -> Protogen {
        match (self.state, self.current_animation) {
            (Some(state), Some(current_animation)) => Protogen {
                state,
                current_animation,
            },
            (_, _) => {
                panic!("tried to build an incomplete protogen");
            }
        }
    }
}

pub struct Protogen {
    state: State,
    current_animation: Animation<State>,
}

impl Protogen {
    pub fn new() -> PartialProtogen {
        PartialProtogen::default()
    }
}

impl Application for Protogen {
    fn setup(self) -> Self {
        // println!("protogen-os v0.0.1-prealpha");
        self
    }

    fn process(self, _dt: u32) -> Self {
        match self.state {
            State::Idle => {}
            State::Blink => {}
        }

        self
    }
}
