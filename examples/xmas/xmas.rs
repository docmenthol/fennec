use bsp::hal::{
    clocks::{init_clocks_and_plls, Clock},
    pac,
    sio::Sio,
    watchdog::Watchdog,
};
use defmt::*;
use defmt_rtt as _;
use embedded_hal::digital::v2::OutputPin;
use fennec::{animation::Animation, os::Application};
use pimoroni_tiny2040 as bsp;
use rp2040_hal::gpio::{
    bank0::{Gpio18, Gpio19, Gpio20},
    Pin, PushPullOutput,
};

#[derive(PartialEq, Eq, Default)]
pub enum State {
    #[default]
    Solid,
    Chase,
}

struct LedPins {
    red: Pin<Gpio18, PushPullOutput>,
    green: Pin<Gpio19, PushPullOutput>,
    blue: Pin<Gpio20, PushPullOutput>,
}

pub struct Xmas<State: Eq> {
    state: State,
    current_animation: Animation<State>,
    delay: cortex_m::delay::Delay,
    led_pins: LedPins,
}

impl Xmas<State> {
    pub fn new() -> Self {
        let mut pac = pac::Peripherals::take().unwrap();
        let core = pac::CorePeripherals::take().unwrap();
        let mut watchdog = Watchdog::new(pac.WATCHDOG);
        let sio = Sio::new(pac.SIO);

        let pins = bsp::Pins::new(
            pac.IO_BANK0,
            pac.PADS_BANK0,
            sio.gpio_bank0,
            &mut pac.RESETS,
        );

        let clocks = init_clocks_and_plls(
            bsp::XOSC_CRYSTAL_FREQ,
            pac.XOSC,
            pac.CLOCKS,
            pac.PLL_SYS,
            pac.PLL_USB,
            &mut pac.RESETS,
            &mut watchdog,
        )
        .ok()
        .unwrap();

        let delay = cortex_m::delay::Delay::new(core.SYST, clocks.system_clock.freq().to_Hz());

        let led_red = pins.led_red.into_push_pull_output();
        let led_green = pins.led_green.into_push_pull_output();
        let led_blue = pins.led_blue.into_push_pull_output();

        let led_pins = LedPins {
            red: led_red,
            green: led_green,
            blue: led_blue,
        };

        Xmas {
            state: State::Solid,
            current_animation: Animation {
                name: State::Solid,
                // frames: &[],
                goto: None,
            },
            delay,
            led_pins,
        }
    }
}

impl Application for Xmas<State> {
    fn setup(mut self) -> Self {
        info!("xmas v0.0.1-prealpha");

        self.led_pins.red.set_high().unwrap();
        self.led_pins.green.set_high().unwrap();
        self.led_pins.blue.set_high().unwrap();

        self
    }

    fn process(mut self, _dt: u32) -> Self {
        // match self.state {
        //     State::Solid => {}
        //     State::Chase => {}
        // }

        self.led_pins.green.set_low().unwrap();
        self.delay.delay_ms(100);
        self.led_pins.green.set_high().unwrap();

        info!("blue");
        self.led_pins.blue.set_low().unwrap();
        self.delay.delay_ms(100);
        self.led_pins.blue.set_high().unwrap();

        info!("red");
        self.led_pins.red.set_low().unwrap();
        self.delay.delay_ms(100);
        self.led_pins.red.set_high().unwrap();

        self
    }
}
